import React, { useState, useEffect } from "react";
import { addDetails, getDetails } from "./helper/apicalls";
import { CSVLink } from "react-csv";
import logo from "./assets/loader.gif";

const App = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [mobile, setMobile] = useState("");
  const [gender, setGender] = useState("MALE");
  const [married, setMarried] = useState("MARRIED");
  const [bloodGroup, setBloodGroup] = useState("A");
  const [loading, setLoading] = useState(false);

  const addDetailsFn = () => {
    setLoading(true);
    let dataToSend = {
      name: name,
      email: email,
      mobile: mobile,
      gender: gender,
      married: married,
      bloodGroup: bloodGroup,
    };

    addDetails(dataToSend).then((data) => {
      if (data.err === false) {
        setLoading(false);
        alert("Congratulations! Booking Successful!");
        setName("");
        setEmail("");
        setMobile("");
        setGender("MALE");
        setMarried("MARRIED");
        setBloodGroup("A");
        generateCSV();
      } else {
        setLoading(false);
        alert("Oh! Looks like some error occoured in the process!");
        setName("");
        setEmail("");
        setMobile("");
        setGender("MALE");
        setMarried("MARRIED");
        setBloodGroup("A");
        generateCSV();
      }
    });
  };

  const handleChangeName = (property) => (event) => {
    setName(event.target.value);
  };
  const handleChangeEmail = (property) => (event) => {
    setEmail(event.target.value);
  };
  const handleChangeMobile = (property) => (event) => {
    setMobile(event.target.value);
  };
  const handleChangeGender = (property) => (event) => {
    setGender(event.target.value);
  };
  const handleChangeMarried = (property) => (event) => {
    setMarried(event.target.value);
  };
  const handleChangeBloodGroup = (property) => (event) => {
    setBloodGroup(event.target.value);
  };
  const [csvData, setCsvData] = useState([
    ["NAME", "EMAIL", "MOBILE", "GENDER", "MARRIED", "BLOOD GROUP"],
  ]);

  const generateCSV = () => {
    setCsvData([
      ["NAME", "EMAIL", "MOBILE", "GENDER", "MARRIED", "BLOOD GROUP"],
    ]);
    getDetails()
      .then((data) => {
        data.forEach((resp) => {
          let toAppend = [];
          toAppend.push(resp.name);
          toAppend.push(resp.email);
          toAppend.push(resp.mobile);
          toAppend.push(resp.gender);
          toAppend.push(resp.married);
          toAppend.push(resp.bloodGroup);
          console.log(toAppend);
          setCsvData((csvData) => [...csvData, toAppend]);
        });
        return;
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    generateCSV();
  }, []);
  return (
    <div className="container">
      <br />
      <form>
        <div className="form-group">
          <label for="exampleFormControlSelect1">
            <b>NAME</b>
          </label>
          <input
            type="text"
            className="form-control"
            id="name"
            placeholder="NAME"
            value={name}
            onChange={handleChangeName("name")}
            required
          ></input>
        </div>
        <div className="form-group">
          <label for="exampleFormControlSelect1">
            <b>EMAIL</b>
          </label>
          <input
            type="email"
            className="form-control"
            id="email"
            placeholder="EMAIL"
            value={email}
            onChange={handleChangeEmail("email")}
            required
          ></input>
        </div>
        <div className="form-group">
          <label for="exampleFormControlSelect1">
            <b>MOBILE NUMBER</b>
          </label>
          <input
            type="text"
            className="form-control"
            id="mobile"
            placeholder="MOBILE NUMBER"
            value={mobile}
            onChange={handleChangeMobile("mobile")}
            required
          ></input>
        </div>
        <div className="form-group">
          <label>
            <b>GENDER</b>
          </label>
          <select
            className="form-control"
            id="gender"
            value={gender}
            onChange={handleChangeGender("gender")}
          >
            <option>MALE</option>
            <option>FEMALE</option>
            <option>PREFER NOT TO SAY</option>
          </select>
        </div>

        <div className="form-group">
          <label for="exampleFormControlSelect1">
            <b>MARITIAL STATUS</b>
          </label>
          <select
            className="form-control"
            id="married"
            value={married}
            onChange={handleChangeMarried("married")}
          >
            <option>MARRIED</option>
            <option>NOT MARRIED</option>
          </select>
        </div>

        <div className="form-group">
          <label for="exampleFormControlSelect1">
            <b>BLOOD GROUP</b>
          </label>
          <select
            className="form-control"
            id="bloodGroup"
            value={bloodGroup}
            onChange={handleChangeBloodGroup("bloodGroup")}
          >
            <option>A</option>
            <option>B</option>
            <option>AB</option>
            <option>O</option>
          </select>
        </div>

        <center>
          <button type="button" class="btn btn-dark" onClick={addDetailsFn}>
            SUBMIT
          </button>
        </center>
        {loading && (
          <center>
            <img src={logo} alt="loading..."></img>
          </center>
        )}
        <br />
        <center>
          <CSVLink data={csvData} filename="DATA.csv" class="btn btn-secondary">
            GENERATE CSV
          </CSVLink>
        </center>
      </form>
      <br />
      <br />
      <br />
      <center>
        <h5>
          DEVELOPED BY~
          <a href="https://linkedin.com/in/kartikag1">KARTIK AGARWAL</a>
        </h5>
      </center>
    </div>
  );
};

export default App;
