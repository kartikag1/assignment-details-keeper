import { API } from "../backend";

export const addDetails = (v) => {
  return fetch(`${API}`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(v),
  })
    .then((response) => {
      return response.json();
    })
    .catch((err) => console.log(err));
};

export const getDetails = () => {
  return fetch(`${API}`, {
    method: "GET",
  })
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err);
    });
};
