# DETAILS KEEPER #

This application can be used to keep the details of individuals and generate a csv of the details.

### TECH STACK ###

* MongoDB Atlas
* Express.js
* React.js
* Node.js
* Bootstrap 4

### Application is hosted on Netlify - https://csvgenerator.netlify.app/ ###

### API's are hosted on Heroku  (https://jsontoxls.herokuapp.com/api/)  ###

### Developed By - KARTIK AGARWAL ###
#### LinkedIN - (https://linkedin.com/in/kartikag1) ####
#### GITHUB - (https://github.com/kartikag1) ####
#### PORTFOLIO - (https://kartikfolio.herokuapp.com/) ####