const router = require("express").Router();

const { getAllData, postData, deleteData } = require("../controllers/app");

router.get("/", getAllData);

router.get("/delete", deleteData);

//*********************************************************************************************

router.post("/", postData);

//*********************************************************************************************

module.exports = router;
