const express = require("express");
const mongoose = require("mongoose");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cors = require("cors");

const appRouter = require("./routes/app");

const app = express();

app.use(morgan("combined"));

app.use(bodyParser.json());
app.use(cors());

let MONGO_URI =
  "";
mongoose
  .connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .then(() => {
    console.log("DB CONNECTED ");
  })
  .catch((err) => {
    console.log("DB CONNECTION ERROR => " + err);
  });

app.use("/api", appRouter);

app.use("*", (req, res) => {
  return res.status(404).json({ error: "page not found" });
});

const port = process.env.PORT || 3002;

app.listen(port, () => {
  console.log(`App up and listening on port: ${port} `);
});
