const Data = require("../models/data");
//********************************************************************************
exports.getAllData = (req, res) => {
  Data.find({}, (err, docs) => {
    if (err) {
      return res.status(400).json({ error: "some error occoured in fetching" });
    }
    return res.status(200).json(docs);
  });
};

//********************************************************************************

exports.postData = (req, res) => {
  Data.create(req.body, (err, docs) => {
    if (err) {
      return res.status(400).json({ error: "some error in storing data" });
    }
    return res.status(200).json(docs);
  });
};

//********************************************************************************

exports.deleteData = (req, res) => {
  Data.deleteMany({}, (err, docs) => {
    if (err) {
      return res.status(400).json({ error: "some error in deleting data" });
    }
    return res.status(200).json(docs);
  });
};
