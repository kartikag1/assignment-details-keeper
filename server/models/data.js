let mongoose = require("mongoose");

let dataSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    mobile: {
      type: String,
      required: true,
    },
    gender: {
      type: String,
      required: true,
    },
    married: {
      type: String,
      required: true,
    },
    bloodGroup: {
      type: String,
      required: true,
    },
    err: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Datam", dataSchema);
